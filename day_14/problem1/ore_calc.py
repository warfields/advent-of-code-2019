#!/usr/bin/env python3
import re
from tqdm import tqdm

# {Ingrediet: ([recipe], amount made, Global production total)}
global recipes
recipes = {"ORE": ([], 1)}

# {Reagent: Num}
global stockpile
stockpile = {"ORE": 0}

# {Reagent: Num made}
global reagent_stats
reagent_stats = {}

def cost(result):
    _, result = result.split(" ")[:2]
    total_cost = 0
    global recipes

    for ingredient in recipes[result][0]:

        num_needed = int(ingredient.split(" ")[0])
        num_made = 0
        #before = 0
        in_name = ingredient.split(" ")[1]
        
        ingredient_cost = 0

        if "ORE" in in_name:
            total_cost += num_needed

        else:
            amount_ingredient = stockpile[in_name]
            before = stockpile[in_name]

            while (amount_ingredient < num_needed):
                ingredient_cost = cost(ingredient)
                num_made += recipes[in_name][1]
                amount_ingredient += recipes[in_name][1]
                total_cost += ingredient_cost
            
            amount_ingredient -= num_needed
            stockpile[in_name] = amount_ingredient
            reagent_stats[in_name] += num_made
        
            #print(f"{in_name}:")
            #print(f"    Need {num_needed}")
            #print(f"    Stockpile before {before}")
            #print(f"    Stockpile After {stockpile[in_name]}")
            #print(f"    Num per recipe {recipes[in_name][1]}")
            #print(f"    Num made {num_made}\n")




    return total_cost 

# Read in data
with open("input.txt") as f:
    for line in f:
        #print(line, end="")

        # Remove delimiters and add to recipe, stockpile dicts dict
        line = re.split(", |=> ", line)
        symbol = line[-1].split(" ")
        num, symbol = symbol
        symbol = symbol[:-1]
        recipes[symbol] = (line[:-1], int(num), 0)
        stockpile[symbol] = 0
        reagent_stats[symbol] = 0
        #print(f"{symbol}: {recipes[symbol]}")
        #print()

print("Finding the total cost")

target = 1000000000000
ore_tot = 0
fuel = 0

with tqdm(total=target, unit="ORE", unit_scale = True) as pbar:
    while(ore_tot < target):
        ore_tot += cost("1 FUEL")
        fuel += 1
        pbar.update(ore_tot)

print(stockpile)
print(fuel)

# for ingredient, num in reagent_stats.items():
#     c_in = cost(f"1 {ingredient}")
#     c_in = c_in * num
#     print(f"{ingredient}: {num} Costing: {c_in}")
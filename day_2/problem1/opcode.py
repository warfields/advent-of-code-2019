# https://adventofcode.com/2019/day/2
binary = []

# Read in the input
with open("input.txt") as f:
    for line in f:
        # String conversion with list comprehension
        binary += [int(x) for x in line.split(sep=",")]

print(binary)

for i in range(0, len(binary), 4):
    print(f"{i}: {binary[i]}")
    if binary[i] == 99: # No op
        break

    if binary[i] == 1: # Add
        print(f"ADD: {binary[binary[i+1]]} {binary[binary[i+2]]} = {(binary[binary[i+1]] + binary[binary[i+2]])} -> # {binary[i+3]}")
        binary[binary[i+3]] = (binary[binary[i+1]] + binary[binary[i+2]])
        continue

    if binary[i] == 2:
        print(f"MLT: {binary[binary[i+1]]} {binary[binary[i+2]]} {(binary[binary[i+1]] + binary[binary[i+2]])} -> # {binary[i+3]}")
        binary[binary[i+3]] = binary[binary[i+1]] * binary[binary[i+2]]

for num in binary:
    print(num, end=",")

print("")

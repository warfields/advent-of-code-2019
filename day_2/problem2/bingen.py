#!/usr/bin/env python3
from binparser import Int_Code

binary = []
# Read in the input
with open("input.txt") as f:
    for line in f:
        # String conversion with list comprehension
        binary += [int(x) for x in line.split(sep=",")]

print(binary)

ohea = Int_Code()

ohea.binary = binary

for i in range(0,100):
    for j in range(0,100):
        #print(f"Noun: {i}, Verb: {j}")
        try:
            out = ohea.parse_nv(i,j)
        except IndexError:
            pass
        
        if out == 19690720:
            print(f"Noun: {i}, Verb: {j}")
            break

# https://adventofcode.com/2019/day/2

from copy import deepcopy

class Int_Code:
    def __init__(self):
        self.int_ptr = 0 # Instriuction Pointer
        self.binary = []
    
    # Parses a pure binary
    def parse(self, bin):

        for i in range(0, len(bin), 4):
            #print(f"{i}: {bin[i]}")
            if bin[i] == 99: # No op
                break

            if bin[i] == 1: # Add
                #print(f"ADD: {bin[bin[i+1]]} {bin[bin[i+2]]} {(bin[bin[i+1]] + bin[bin[i+2]])} -> # {bin[i+3]}")
                bin[bin[i+3]] = (bin[bin[i+1]] + bin[bin[i+2]])
                continue

            if bin[i] == 2: # mult
                #print(f"MLT: {bin[bin[i+1]]} {bin[bin[i+2]]} {(bin[bin[i+1]] + bin[bin[i+2]])} -> # {bin[i+3]}")
                bin[bin[i+3]] = bin[bin[i+1]] * bin[bin[i+2]]

        #for num in bin:
        #    print(num, end=",")
        return bin

    # Parses the curently set binary with a verb/noun combo
    def parse_nv(self, noun, verb):

        newbin = deepcopy(self.binary)
        newbin[1] = noun
        newbin[2] = verb
        newbin = self.parse(newbin)

        return newbin[0]

#!/usr/bin/env python3
# https://adventofcode.com/2019/day/3
from itertools import starmap
# Parse File
fuel_lines = []

with open("input.txt") as f:
    for line in f:
        # String conversion with list comprehension
        fuel_lines.append(line.splitlines()[0].split(sep=","))

fuel_lines = fuel_lines[:-1]
# print(fuel_lines)

board = {}
dirs = {'R': (-1,0), 'L':(1,0), 'U':(0,1), 'D':(0,-1)}
intersections = []

for line_num in range(len(fuel_lines)):
    line = fuel_lines[line_num]
    cur_loc = (0,0)
    line_len = 0

    for bend in line:
        direction = bend[0]
        for i in range(int(bend[1::])):
            line_len += 1
            # Add the direction to the cur_loc tuple
            cur_loc = tuple(map(sum,zip(cur_loc,dirs[direction])))
            # print(cur_loc)

            # See if it is it the board
            if cur_loc in board:
                if board[cur_loc][0] == line_num:
                    continue
                intersections.append(cur_loc)
            else:
                board[cur_loc] = (line_num, line_len)

# print(intersections)
print(min([sum((map(abs, loc))) for loc in intersections]))

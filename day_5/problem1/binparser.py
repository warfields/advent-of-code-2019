# https://adventofcode.com/2019/day/2

from copy import deepcopy
from collections import defaultdict

class Int_Code:
    def __init__(self):
        self.inst_ptr = 0 # Instruction Pointer
        self.binary = []
        self.input_reg = 0 # The Int Code machine will raise an io exception
        self.output_reg = 0
    
        # {instruction:  date length}
        self.opcode_table = defaultdict(3)
        self.opcode_table[3] = 1
        self.opcode_table[4] = 1

        # exicution table

    # Parses a pure binary
    def parse(self):
        bin = self.binary

        while self.inst_ptr <= len(bin):
            ptr = self.inst_ptr
            if bin[ptr] == 99: # No op
                break

            # Instruction fetch
            instr = str(bin[ptr])
            if len(instr) > 2:
                opcode = int(instr[-2])
            else:
                opcode = int(instr)

            instr = instr.zfill(self.opcode_table[opcode] + 2)
            data = [int(x) for x in instr[:-2]]

    
        self.inst_ptr = 0
        return bin

    # Parses the curently set binary with a verb/noun combo
    def parse_nv(self, noun, verb, stepwise=False):

        newbin = deepcopy(self.binary)
        self.binary[1] = noun
        self.binary[2] = verb
        self.binary = self.parse()

        returner = self.binary
        self.binary = newbin

        return returner

    def run_step():
        ...

    def exec_instruction(function, bin, data=[], return_addr)